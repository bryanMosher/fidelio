#!/bin/bash

# sudo apt-get install git cheese sl mc tmux zsh \
# curl xbacklight weather-util keepass2 apache2 \
# python-pip npm xbacklight redshift mplayer
# sudo apt-get install libmtp-common mtp-tools libmtp-dev libmtp-runtime libmtp9

# pip install grip

cd ~

#gits

#git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

#git clone https://github.com/phiggins/konsole-colors-solarized ~/.gitpile/solarized/
#ln -s ~/.gitpile/solarized/Solarized\ Dark.colorscheme ~/.kde4/share/apps/konsole/Solarized-dark.colorscheme
#ln -s ~/.gitpile/solarized/Solarized\ Light.colorscheme ~/.kde4/share/apps/konsole/Solarized-light.colorscheme

#git clone https://github.com/powerline/fonts.git ~/.gitpile/patchedfonts/
#~/.gitpile/patchedfonts/install.sh

#youtube-dl

#git clone https://bryanMosher@bitbucket.org/bryanMosher/fidelio.git ~/.gitpile/fidelio

#zsh
#sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
#chsh -s /bin/zsh

#ln -s ~/.gitpile/fidelio/dotfiles/.vimrc ~/.vimrc
#ln -s ~/.gitpile/fidelio/dotfiles/alias.zsh ~/.oh-my-zsh/lib/alias.zsh
#ln -s ~/.gitpile/fidelio/dotfiles/.tmux.conf ~/.tmux.conf
#ln -s ~/.gitpile/fidelio/dotfiles/.zshrc ~/.zshrc
#ln -s ~/.gitpile/fidelio/dotfiles/.xmodmap-beemo ~/.Xmodmap
#ln -s ~/.gitpile/fidelio/dotfiles/.gitconfig ~/.gitconfig
#ln -s ~/.gitpile/fidelio/dotfiles/.vimperatorrc ~/.vimperatorrc
#ln -s ~/.gitpile/fidelio/dotfiles/redshift.conf ~/.config/redshift.conf
# echo 2 | sudo tee /sys/module/hid_apple/parameters/fnmode
