set nocompatible " be iMproved, required
filetype off " required
let mapleader = "\<Space>" " general prefix key
set rtp+=~/.vim/bundle/Vundle.vim " vundle business
set t_Co=16
call vundle#begin()
let g:solarized_termtrans=1
Plugin 'samsonw/vim-task' " taskmgr
Plugin 'gmarik/Vundle.vim' " maintain the plugin manager
Plugin 'xolox/vim-session' " session saving and restoring
Plugin 'xolox/vim-misc' " other xolox depends
Plugin 'tpope/vim-vinegar' " - invokes netrw et al
Plugin 'christoomey/vim-tmux-navigator' " easy to travel between tmux and vim panes
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'tpope/vim-fugitive' " git wrapper
Plugin 'tpope/vim-commentary' " comment functions
Plugin 'tpope/vim-abolish' " works with variants of words
Plugin 'tpope/vim-surround' "surrounding with () etc
Plugin 'airblade/vim-gitgutter' " git glyphs when in a repo dir editing a tracked file
Plugin 'sjl/gundo.vim' " undo tree and history
Plugin 'mattn/emmet-vim' " expansion
Plugin 'godlygeek/tabular' " spacing and tabbing plaintext tables
Plugin 'tpope/vim-repeat'
Plugin 'YankRing.vim' " yank history
Plugin 'tpope/vim-unimpaired' " cool keyhooks
Plugin 'easymotion/vim-easymotion' " teleportation by keysequence
Plugin 'altercation/vim-colors-solarized'
call vundle#end()
filetype plugin indent on

" plugin specific settings
set background=dark
colorscheme solarized
let g:airline_powerline_fonts = 1 "fancy glyphs
" set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
" set statusline+=%*
" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 1
" let g:syntastic_check_on_open = 1
" let g:syntastic_check_on_wq = 0
let g:ctrlp_show_hidden = 1
" let g:yankring_min_element_length = 2 "no tiny yanks
" make the statusbar show when there is only one pane
set laststatus=2
inoremap <silent> <buffer> <C-\> <ESC>:call Toggle_task_status()<CR>i
noremap <silent> <buffer> <C-\> :call Toggle_task_status()<CR>


" let g:airline_theme='airline-colors-solarized'
" let g:airline_theme='dark'
let g:airline_theme = 'base16_solarized'
let g:airline_section_error = 0
let g:airline_section_warning = 0

" disabled plugins
" Plugin 'tpope/vim-speeddating'
" Plugin 'flazz/vim-colorschemes'
" Plugin 'jmcantrell/vim-virtualenv'
" Plugin 'terryma/vim-multiple-cursors'
" Plugin 'Valloric/YouCompleteMe'
" Plugin 'scrooloose/syntastic' " syntax checking 
" Plugin 'tpope/vim-sensible' " reasonable settings consider disabling
" Plugin 'kien/ctrlp.vim' "fuzzy file find

" Settings that never change
set encoding=utf-8
set rnu "i like relative numbering and absolute numbering
set nu
set cpoptions+=$ " the dollar sign on ciw etc.
set cpoptions+=y " a yank command can be redone with "."
set modeline "settings inside files
set foldmarker={{{,}}} "this is how i fold
set foldmethod=marker "option could be indent " instead but set bu modeline
set wildmenu "dope wildcard menu for help
set showcmd "if there is a pending keyed command show it
set incsearch "search as i type
set hlsearch "at least initially highlight the current search results
set smartcase "case insensitive search except when you use a capital
set scrolloff=1 "kinda center the cursor but not too much
set tabstop=2 "tiny indents
set noexpandtab "keep them tabs and not spaces
set softtabstop=2
set shiftwidth=2
set autoindent "keep the indent going
set visualbell "visual flash if no sound

"what is considered a Word
set iskeyword+=95 "_
"set iskeyword+=46 ".
set iskeyword+=45 "-
" set iskeyword+=36 "$
" set iskeyword+=37 "%

" au FocusLost * :wa " save when I tab away

" defaul commands that i dont like
nmap <C-a> <nop>
nmap <C-x> <nop>
nnoremap K <nop>
nnoremap Q <nop>

" set linebreak=""
" nifty ideas
" nnoremap J mzJ`z
" nnoremap n nzz
nnoremap <silent> <leader>lx :WC<CR>
command! WC write | !pdflatex %

" nnoremap j gj
" nnoremap k gk
" nnoremap gj j
" nnoremap gk k
set backupdir=~/.vim/backup
set directory=~/.vim/swap

" shortened messages at the bottom
" truncate modified newfile readonly and written
" also skip the Intro sorry Bram
set shortmess+=mnrwIa

" make backspage work normally
set backspace=indent,eol,start

" set gdefault for always g on s commangs
" set virtualedit=block to make it go in blank
" space
" new splits go where 
set splitbelow
set splitright

nnoremap <silent> <leader>gs :Gstatus<CR>
nnoremap <silent> <leader>gd :Gdiff<CR>
nnoremap <silent> <leader>gc :Gcommit --verbose<CR>
" nnoremap <silent> <leader>gam :Gcommit --verbose --amend<CR>
" nnoremap <silent> <leader>gb :Gblame<CR>
nnoremap <silent> <leader>gl :Glog<CR>
nnoremap <silent> <leader>gp :Git push<CR>
nnoremap <silent> <leader>gw :Gwrite<CR>
" nnoremap <silent> <leader>gbr :Git branch<CR>

" set shiftround
" set textwidth=0
" set nobackup
" set nowritebackup
" set noswapfile
" set history=999
" set scrolloff=10
" set gdefault
" set whichwrap+=<,>,[,]
" set title
" set ttyfast
" set virtualedit=onemore
" set fileencodings=.
set listchars=tab:÷÷,trail:×,eol:¬
"Дξ♫♫░Ω
set hidden
set list
"set linebreak
"set cpoptions+=n
"set foldcolumn=1
set mouse=
set statusline=%<%f\ %h%m%r%{fugitive#statusline()}%=%-14.(%l,%c%V%)\ %P
" prevent command needing to press enter for more
" set cmdheight=2

" set guicursor=a:blinkwait100-blinkon150-blinkoff100
" nnoremap <leader>l :set list!<CR>

"add more of these
nnoremap <leader>ev :tabe $MYVIMRC<cr>
nnoremap <leader>ezr :tabe ~/dotFiles/.zshrc<cr>
nnoremap <leader>eza :tabe ~/dotFiles/alias.zsh<cr>
nnoremap <leader>et :tabe ~/dotFiles/.tmux.conf<cr>
nnoremap <leader>er :tabe ~/dotFiles/.ratpoisonrc<cr>

"window manipulations
nnoremap <leader>w 

nnoremap <leader>bu :!xbacklight -inc 5
nnoremap <leader>bd :!xbacklight -dec 5

set guifont=Anonymous\ Pro\ for\ Powerline
nnoremap <leader>s<leader> :w<CR>
nnoremap <leader>5 :so ~/.vimrc<CR>
nnoremap <leader>3 :b#<CR>
nnoremap <leader>t :tabe<CR>
nnoremap <leader>u<leader> 
nnoremap <leader>8 :set guifont=*<CR>
vnoremap <Leader>y "+y
vnoremap <Leader>d "+d
nnoremap <Leader>y "+y
nnoremap <Leader>d "+d
nnoremap <Leader>p :set paste<CR>"+p:set nopaste<CR>
nnoremap <Leader>P :set paste<CR>"+P:set nopaste<CR>
vnoremap <Leader>p :set paste<CR>"+p:set nopaste<CR>
vnoremap <Leader>P :set paste<CR>"+P:set nopaste<CR>
nnoremap <leader>dm Vyo=";
nnoremap Y y$
inoremap jk <esc>l
" vnoremap kjk <esc> too annoying to handle
nnoremap <F1> <nop>
" nnoremap <silent> <F2> :cal VimCommanderToggle()<CR>
" nnoremap <silent> <F3> :execute ':CtrlPLastMode --dir'<CR>
nnoremap <silent> <F2> :YRShow<CR>
inoremap <F2> :YRShow<CR>
nnoremap <silent> <F3> :GundoToggle<CR>
" nnoremap <silent> <F6> :echo "hjkl"<CR>
" nnoremap <silent> <F7> :echo "hjkl"<CR>
" nnoremap <silent> <F8> :echo "hjkl"<CR>
" nnoremap <silent> <F9> :echo "hjkl"<CR>
" nnoremap <silent> <F10> :echo "hjkl"<CR>
" noremap gV `[v`]
" nnoremap _ +
autocmd BufWritePre,BufRead *.html :normal gg=G
autocmd BufWritePre,BufRead *.scss :normal gg=G
autocmd BufWritePre,BufRead *.css :normal gg=G

nnoremap <silent> <Leader>ap :Tabularize /\|<CR>
vnoremap <silent> <Leader>ap :Tabularize /\|<CR>
nnoremap <silent> <Leader>a: :Tabularize /:\zs<CR>
vnoremap <silent> <Leader>a: :Tabularize /:\zs<CR>
nnoremap <silent> <Leader>a= :Tabularize /=<CR>
vnoremap <silent> <Leader>a= :Tabularize /=<CR>
nnoremap <silent> <Leader>a/ :Tabularize /\/\/<CR>
vnoremap <silent> <Leader>a/ :Tabularize /\/\/<CR>
nnoremap <silent> <Leader>a& :Tabularize /&<CR>
vnoremap <silent> <Leader>a& :Tabularize /&<CR>

let g:ctrlp_max_height=30
let g:ctrlp_max_files = 1000
let g:ctrlp_max_depth = 10
let g:ctrlp_lazy_update = 1
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*.pyc
" let g:ctrlp_extensions = ['funky']
" nnoremap <Leader>cf :CtrlPFunky<Cr>
let g:ctrlp_use_caching = 1
" let g:ctrlp_by_filename = 1
" let g:ctrlp_show_hidden = 1
" let g:ctrlp_working_path_mode = 'ra'
" better tabbing
" vnoremap < <gv
" vnoremap > >gv
" spell check ',s'
" nmap <silent> <leader>s :set spell!<CR>
" map <leader>sn ]s
" map <leader>sp [s
" map <leader>sa zg
" map <leader>s? z=
" strip all whitespace with ,WW
" nnoremap <leader>WW :%s/\s\+$//<cr>:let @/=''<CR>
" nmap <Leader>a:: :Tabularize /:zs<CR>
" vmap <Leader>a:: :Tabularize /:zs<CR>
" nmap <Leader>a, :Tabularize /,<CR>
" vmap <Leader>a, :Tabularize /,<CR>
" nmap <Leader>a,, :Tabularize /,zs<CR>
" vmap <Leader>a,, :Tabularize /,zs<CR>
" nmap <Leader>a<Bar> :Tabularize /<Bar><CR>
" vmap <Leader>a<Bar> :Tabularize /<Bar><CR>
syntax enable
"https://gist.github.com/287147 for insert mode auto tabularize
"help listchars
set autochdir
" python filetype tab config
autocmd FileType python set sw=4
autocmd FileType python set ts=4
autocmd FileType python set sts=4
autocmd FileType htmldjango set foldmarker={}{}{,}{}{}
autocmd FileType markdown set tw=50
" surround extra mappings
" let b:surround_{char2nr("v")} = "{{ \r }}"
" let b:surround_{char2nr("{")} = "{{ \r }}"
" let b:surround_{char2nr("%")} = "{% \r %}"
" let b:surround_{char2nr("b")} = "{% block \1block name: \1 %}\r{% endblock \1\1 %}"
" let b:surround_{char2nr("i")} = "{% if \1condition: \1 %}\r{% endif %}"
" let b:surround_{char2nr("w")} = "{% with \1with: \1 %}\r{% endwith %}"
" let b:surround_{char2nr("f")} = "{% for \1for loop: \1 %}\r{% endfor %}"
" let b:surround_{char2nr("c")} = "{% comment %}\r{% endcomment %}""}")}

function! YRRunAfterMaps()
	nnoremap Y :<C-U>YRYankCount 'y$'<cr>
endfunction

" manipulating md headers
nnoremap <leader>hk ^i#<esc>$
nnoremap <leader>bk ^i- <esc>$
nnoremap <leader>uk lbi_<esc>Ea_<esc>hh
nnoremap <leader>tt mz0x`z
nnoremap <leader>dp T\|ct\|
nnoremap <leader>ds :%s/\s*$//<cr>
nnoremap <leader>ss :SaveSession
nnoremap <leader>M <Plug>(easymotion-jumptoanywhere)
" only organized vimscript below this line
execute "normal \<Plug>easymotion-jumptoanywhere"
nmap s <Plug>(easymotion-s2)
map <leader>j <Plug>(easymotion-j)
map <leader>k <Plug>(easymotion-k)
map <leader><leader>w <Plug>(easymotion-bd-w)
map <leader><leader>b <Plug>(easymotion-bd-b)
map <leader><leader>e <Plug>(easymotion-bd-e)
map <leader><leader>s <Plug>(easymotion-jumptoanywhere)

" sets colorscheme
let g:ctrlp_show_hidden = 1
nnoremap <leader>m :m+
nnoremap <leader>M :m-
