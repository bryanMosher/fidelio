# Globals
DOTDIR=~/.gitpile/fidelio/dotfiles/
ZSHDIR=~/.oh-my-zsh
# BRYAN=192.168.2.10
# BEEMO=192.168.2.2

alias goot="surfraw google -t"
alias goog="surfraw google"
alias xmomap="xmodmap ~/.Xmodmap"

alias remotemount='sshfs bryan@$BRYAN:/ ~/remote'

alias dotf='cd $DOTDIR'

alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias mk='mkdir'
alias mv='mv -i'
alias rscp="rsync -aP --no-whole-file --inplace"
alias rsmv="rscp --remove-source-files"

# media getting and playing aliases
alias mplay="mplayer -vo aa -quiet -framedrop -monitorpixelaspect"
alias mplaycaca="mplayer -vo caca -quiet -framedrop -monitorpixelaspect"
alias ytdlx="youtube-dl -x --default-search 'ytsearch:'"
alias ytdl="youtube-dl --default-search 'ytsearch:'"

alias v="vim"
alias y="youtube-dl"

alias dingus="tmux new-session -s dingus"

# aliases for Tmux
alias tmux='TERM=xterm-256color /usr/bin/tmux'
alias ta='tmux attach -t'
alias tnew='tmux new -s'
alias tls='tmux ls'
alias tkill='tmux kill-session -t'

# convenience aliases for editing configs
alias v='vim'
alias ev='vim $DOTDIR/.vimrc'
alias et='vim $DOTDIR/.tmux.conf'
alias ez='vim $DOTDIR/.zshrc'
alias ea='vim $DOTDIR/alias.zsh'
alias eT='vim $HOME/todo.md'
alias sz='source ~/.zshrc && echo "source ~/.zshrc"'

# aliases for brightness settings
alias bak='xbacklight -set'

# aliases for color temp adjustments
alias kkember='redshift -O 1200 && echo "Color temperature is now 1200K"'
alias kksunset='redshift -O 1850 && echo "Color temperature is now 1850K"'
alias kkincandescent='redshift -O 2400 && echo "Color temperature is now 2400K"'
alias kkwarmcfl='redshift -O 2700 && echo "Color temperature is now 2700K"'
alias kkafternoon='redshift -O 4300 && echo "Color temperature is now 4300K"'
alias kknoon='redshift -O 5600 && echo "Color temperature is now 5600K"'
alias kkovercast='redshift -O 6500 && echo "Color temperature is now 6500K"'
alias kkpolesky='redshift -O 9999 && echo "Color temperature is now 9999K"'

alias apts='apt-cache search'
alias aptupg='sudo apt-get upgrade'
alias apti='sudo apt-get install'
alias aptin='sudo apt-get install'
alias aptr='sudo apt-get remove'
alias aptp='sudo apt-get purge'
alias aptr='sudo apt-get remove'
alias aptup='sudo apt-get update'
alias aptcl='sudo apt-get autoclean && sudo apt-get clean'
alias aptar='sudo apt-get autoremove'

alias ga='git add'
alias gcm='git commit -m'
alias gc='git commit -v'
alias gcl='git clone'
alias go='git checkout'
alias gra='git remote add'
alias grr='git remote rm'
alias gr='git remote'
alias grv='git remote -v'
alias gp='git push'
alias gundo='git reset --soft HEAD'
alias gundoh='git reset --hard HEAD'
alias .c='git add --all && git commit -v'
alias gi='git init'
alias gst='git status'
alias gs='git status -sb'
alias gla="git log --all --graph \
           --pretty=format:'%Cred%h%Creset \
           -%C(yellow)%d%Creset %s %Cgreen(%cr) \
           %C(bold blue)<%an>%Creset' \
           --abbrev-commit --date=relative"

alias x='extract'
extract (){
    # never forget how to run a tar xyz command again
    # alias to 'x' for even more tastyness
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)   tar xjf $1;;
            *.tar.gz)    tar xzf $1;;
            *.bz2)       bunzip2 $1;;
            *.rar)       rar x $1;;
            *.gz)        gunzip $1;;
            *.tar)       tar xf $1;;
            *.tbz2)      tar xjf $1;;
            *.tgz)       tar xzf $1;;
            *.zip)       unzip $1;;
            *.Z)         uncompress $1;;
            *.7z)        7z x $1;;
            *.xz)        tar xf $1;;
            *)           echo "'$1' cannot be extracted via extract()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}
# # get public ip
# # souce: https://github.com/jleclanche/dotfiles/blob/master/.zshrc#L281
function myip {
    local api
    case "$1" in
        "-4")
            api="http://v4.ipv6-test.com/api/myip.php"
            ;;
        "-6")
            api="http://v6.ipv6-test.com/api/myip.php"
            ;;
        *)
            api="http://ipv6-test.com/api/myip.php"
            ;;
    esac
    curl -s "$api"
    echo # Newline.
}
# alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
# ## # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # globals
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# USER=`whoami`
# BASEDIR=~/dotfiles

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # aliases
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# alias ddm='./manage.py migrate'
# alias dsm='./manage.py sqlmigrate'
# alias dmm='./manage.py makemigrations'
# alias dch='./manage.py check'
# alias dsh='./manage.py shell'
# alias drs='./manage.py runserver'
# alias rbstr='rainbowstream'
# alias qotd="python3 -c 'import wikiquote; print(wikiquote.quote_of_the_day())'"
# alias jrepl='java -jar idea-IC-141.1532.4/javarepl.jar'
# alias setjava8='sudo update-java-alternatives -s java-8-oracle'
# alias setjava8env='sudo apt-get install oracle-java8-set-default'
# alias setjava7='sudo update-java-alternatives -s java-7-oracle'
# alias setjava7env='sudo apt-get install oracle-java7-set-default'
# alias whereis2='dpkg -L'
# alias rrdb='rake db:reset db:migrate'
# alias rs='rails s'
# alias df='df -aH .'
# alias py="python"
# alias py2='python2'
# alias py3='python3'
# alias pip2='pip'
# alias pip3='pip3'
# alias ipy='ipython'
# alias pserve='python -m SimpleHTTPServer 5000'
# alias tede='trans en:de'
# alias tden='trans de:en'
# alias ls='ls -FCA --color=tty'
# alias s='ls'
# alias rmr='rm -rI'
# alias e='exit'
# alias G='| grep -i'
# alias grep='grep -i'
# alias ssheval='eval `ssh-agent -s`'
# alias ly='lynx -vikeys'
# alias nx='PATH=$(npm bin):$PATH'
# alias irssi='TERM=screen-256color irssi'
# alias dall='dpkg --get-selections'
# alias geml='gem list --local'
# alias pyx='py -x'
# alias pyl='py -l'
# alias vssh='vagrant ssh'
# alias -g L='| less'
# alias -g H='| head'
# alias -g S='| sort'
# alias -g T='| tail'
# alias -g SO='1> /dev/null'
# alias -g SE='2> /dev/null'
# alias aptsh='apt-cache show'
# alias epp='echo $PYTHONPATH'
# alias et='vim ~/.tmux.conf'
# alias ei='vim ~/.irssi/config'
# alias ew='cd ~/work/'

# # remove all unwatched files in a git repo
# alias gdel='git status -s | cut -c4- | xargs rm -r $1'

# alias gfucku='git push -f'
# alias gb='git branch'
# alias gow='git show'
# alias gbd='git branch -d'
# alias gbD='git branch -D'
# alias gbr='git branch -r'
# alias gab='git branch -a'
# alias gd='git diff'
# alias gob='git checkout -b'
# alias gh='git hist'
# alias gms='git merge --squash'
# alias gpu='git push -h'
# alias gp='git push'
# alias gher='git push heroku master'
# alias gpl='git pull --rebase'
# alias gplu='git pull -u --rebase'
# alias gam='git commit --amend'
# alias gamm='git commit --amend -m'
# alias gl='git log'
# alias gcv='git cherry -v'
# alias gun='git ls-files --others --exclude-standard'
# alias gcn='git shortlog -s -n'
# alias tmux='tmux -2'
# alias ta='tmux attach -t'
# alias tnew='tmux new -s'
# alias tls='tmux ls'
# alias tkill='tmux kill-session -t'
# alias snew='screen -S'
# alias sls='screen -ls'
# alias sa='screen -x'

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # functions
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# add_to_pp(){
#     # add the first positional parameter to $PYTHONPATH
#     export PYTHONPATH=$PYTHONPATH:$1;
# }

# copyid(){
#     # as user '$1' at IP '$2', copy over our local ssh key
#     # so we don't have to do type a password everytime we
#     # ssh to this machine
#     ssh-copy-id -i ~/.ssh/id_rsa.pub $1@$2
# }

# f(){
#     # quick find. Takes one parameter - the search term
#     echo "find . -iname \"*$1*\""
#     sudo find . -iname "*$1*"
# }


# # make a backup of a file
# # https://github.com/grml/grml-etc-core/blob/master/etc/zsh/zshrc
# bk() {
#     cp -a "$1" "${1}_$(date --iso-8601=seconds)"
# }

# # print terminal-wide banner
# # souce: https://github.com/jleclanche/dotfiles/blob/master/.zshrc#L281
# function hr {
#     print ${(l:COLUMNS::=:)}
# }


# # remove all installed GHC/cabal packages, leaving ~/.cabal binaries and docs
# # in place. When all else fails, use this to get out of dependency hell and 
# # start over - https://gist.github.com/timmytofu/7417408
# function ghc-pkg-reset() {
#   if [[ $(readlink -f /proc/$$/exe) =~ zsh ]]; then
#     read 'ans?Erasing all your user ghc and cabal packages - are you sure (y/N)? '
#   else # assume bash/bash compatible otherwise
#     read -p 'Erasing all your user ghc and cabal packages - are you sure (y/N)? ' ans
#   fi
 
#   [[ x$ans =~ "xy" ]] && ( \
#     echo 'erasing directories under ~/.ghc'; command rm -rf `find ~/.ghc/* -maxdepth 1 -type d`; \
#     echo 'erasing ~/.cabal/lib'; command rm -rf ~/.cabal/lib; \
#   )
# }
