export TERM="xterm-256color"
# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

source ~/.oh-my-zsh/lib/alias.zsh

# export HTTP_PROXY=http://proxy.nycboe.org/proxy.pac:8002
# export HTTPS_PROXY=http://proxy.nycboe.org/proxy.pac:8002
# export http_proxy=http://proxy.nycboe.org/proxy.pac:8002
# export https_proxy=http://proxy.nycboe.org/proxy.pac:8002

# function path putting the custom one first
fpath=( ~/.zfunc "${fpath[@]}" )
autoload -Uz strip
autoload -Uz stripnew

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
# ZSH_THEME="ys"
 ZSH_THEME="powerlevel9k/powerlevel9k"
# ZSH_THEME="robbyrussell"


# autocompletion treats - _ the same
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
# nyan bower django debian systemd pip
plugins=(colored-man cp extract git lol python battery emoji-clock rand-quote themes)

# User configuration

export PATH=$HOME/bin:/usr/local/bin:$PATH
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='mvim'
fi

# Vi mode
bindkey -v
export KEYTIMEOUT=20

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.  For a full list of active aliases, run `alias`.  Example aliases
alias zshconfig="gvim ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# want your terminal to support 256 color schemes? I do ...
export TERM="xterm-256color"

# if you do a 'rm *', Zsh will give you a sanity check!
setopt RM_STAR_WAIT

# allows you to type Bash style comments on your command line
# good 'ol Bash
setopt interactivecomments

# Zsh has a spelling corrector
setopt CORRECT

# sneaky cd
setopt AUTO_CD

# history only delete dupes of history when it starts filling up
setopt HIST_EXPIRE_DUPS_FIRST



# make sure that if a program wants you to edit
# text, that Vim is going to be there for you
export EDITOR="vim"
export USE_EDITOR=$EDITOR
export VISUAL=$EDITOR
# export WORKON_HOME=$HOME/.virtualenvs/
# export PROJECT_HOME=$HOME/banana/
# export VIRTUALENVWRAPPER_SCRIPT=/usr/local/bin/virtualenvwrapper.sh
# source /usr/local/bin/virtualenvwrapper_lazy.sh


# MC_SKIN=/home/beemo/.mc/lib/mc-solarized/solarized.ini

# function switch-theme-night() {
#   konsoleprofile colors=Solarized
# }

# Konsole color changing
theme-night() {
  switch-term-color "colors=Solarized"
}
theme-light() {
  switch-term-color "colors=SolarizedLight"
}
switch-term-color() {
  arg="${1:-colors=Solarized}"
  if [[ -z "$TMUX" ]]
  then
    konsoleprofile "$arg"
  else
    printf '\033Ptmux;\033\033]50;%s\007\033\\' "$arg"
  fi
}

bindkey -M viins 'jk' vi-cmd-mode

list-colors() {
	for i in {0..255} ; do
		printf "\x1b[38;5;${i}mcolour${i}\n"
	done
}
